library verilog;
use verilog.vl_types.all;
entity teste_ula_1bit_vlg_check_tst is
    port(
        CarryOut        : in     vl_logic;
        R               : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end teste_ula_1bit_vlg_check_tst;
