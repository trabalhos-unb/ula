library verilog;
use verilog.vl_types.all;
entity teste_fios_vlg_check_tst is
    port(
        C               : in     vl_logic;
        D               : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end teste_fios_vlg_check_tst;
